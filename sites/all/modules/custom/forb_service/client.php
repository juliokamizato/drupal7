<?php
require_once 'nusoap/lib/nusoap.php';

//Inicia o servico client
$wsdl = 'http://localhost:8888/drupal7/sites/all/modules/custom/forb_service/webservice.php?wsdl';
$client = new nusoap_client($wsdl, true);
$error  = $client->getError();


//Chama funcao Verificar (tem que retornar true)
$verificar = $client->call('Verificar');
/*
 * Se quiser ver o Request e o Result no Format XML, descomente
echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>';
*/

//Criei um arquivo xml do exemplo que o Arnaldo me passou por email
//$requisicao = file_get_contents('test.xml');
$requisicao = "asdfasdfadsfadsf";

if ($verificar) {
    $parametros = array(
        'usuario' => 'julio', //Pode ir vazio, nao tem problema
        'senha' => '1234', // Pode ir vazio
        'requisicao' => "<![CDATA[" . $requisicao . "]]>"
        );

    $postar = $client->call('Postar', $parametros);
}

/*
 * Se quiser ver Request e Result no formato XML do postar, descomente aqui.*/
echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>';