<?php
include_once 'drupal_bootstrap.php';

/**
 * Created by PhpStorm.
 * User: julio
 * Date: 5/11/16
 * Time: 12:10 AM
 */
function Postar($usuario, $senha, $requisicao) {
    $xml = simplexml_load_string($requisicao);
    if ($xml === false) {
        $sucesso = false;
        // Ocorreu um erro
        $mensagem = "";
        foreach(libxml_get_errors() as $error) {
            $mensagem .= urldecode(http_build_query((array) $error, '', ';')) . '\n';
        }
    } else {

        // TODO: Fazer parser de xml para $objeto
        // $node = node_save($objeto);
        //TODO: Fazer node_save
        $sucesso = true;
        $mensagem = 'Integração ocorrida com sucesso';
    }

    return array('Sucesso' => $sucesso, 'Mensagem' => $mensagem);
}


$xml_string = <<<XMLSTRING
<?xml version="1.0" encoding="utf-8"?>
<MENSAGEM>
  <CABECALHO>
    <IdentidadeEmissor>DBFC273E-4811-40C4-8A4E-1629731ADD9A</IdentidadeEmissor>
    <NumeroOperacao>A M Machado  Me</NumeroOperacao>
    <CodigoMensagem>MSG0072</CodigoMensagem>
  </CABECALHO>
  <CONTEUDO>
    <MSG0072>
      <CodigoConta>93af4472-304b-e511-941c-00155d014524</CodigoConta>
      <CodigoCliente>214650</CodigoCliente>
      <NomeRazaoSocial>A M Machado  Me</NomeRazaoSocial>
      <NomeFantasia>A M Machado  Me</NomeFantasia>
      <NomeAbreviado>103548330001</NomeAbreviado>
      <TipoRelacao>993520000</TipoRelacao>
      <Telefone>9888315777</Telefone>
      <Email>asdfasd@dfads.com</Email>
      <Natureza>993520000</Natureza>
      <CNPJ>10354833000104</CNPJ>
      <InscricaoEstadual>ISENTO</InscricaoEstadual>
      <ContribuinteICMS>false</ContribuinteICMS>
      <DataConstituicao>2016-03-29</DataConstituicao>
      <DistribuidorPrincipal>f7e8e1af-d500-e411-9420-00155d013d39</DistribuidorPrincipal>
      <Exclusividade>false</Exclusividade>
      <ParticipaProgramaCanais>993520001</ParticipaProgramaCanais>
      <RamoAtividadeEconomica>cultivo de arroz</RamoAtividadeEconomica>
      <CNAE>01113</CNAE>
      <Situacao>0</Situacao>
      <Classificacao>46c0815a-6dd9-e511-8c4b-0050568d7c5e</Classificacao>
      <SubClassificacao>a64c08e7-6dd9-e511-8c4b-0050568d7c5e</SubClassificacao>
      <NivelPosVenda>37e3a262-75ed-e311-9407-00155d013d38</NivelPosVenda>
      <ContatoPrincipal>28cbd3c0-ebf5-e511-a468-0050568d7c5e</ContatoPrincipal>
      <ApuracaoBeneficio>993520000</ApuracaoBeneficio>
      <TipoConstituicao>993520001</TipoConstituicao>
      <Proprietario>96af4472-304b-e511-941c-00155d014524</Proprietario>
      <TipoProprietario>team</TipoProprietario>
      <TipoConta>993520000</TipoConta>
      <DataAdesao>2016-03-30</DataAdesao>
      <OrigemConta>993520000</OrigemConta>
      <StatusIntegracaoSefaz>993520004</StatusIntegracaoSefaz>
      <DataBaixaContribuinte>2016-03-29</DataBaixaContribuinte>
      <AssistenciaTecnica>false</AssistenciaTecnica>
      <CodigoRamoAtividadeEconomica>baf87ce7-2ee6-e511-a468-0050568d7c5e</CodigoRamoAtividadeEconomica>
      <ParticipaProgramaCanaisMotivo>993520001</ParticipaProgramaCanaisMotivo>
      <AdesaoPciRealizadaPor>Anerson tesets teset</AdesaoPciRealizadaPor>
      <EscolheuDistrForaSellOut>false</EscolheuDistrForaSellOut>
      <DataUltimoSellOut>2016-02-03</DataUltimoSellOut>
      <EnderecoPrincipal>
        <TipoEndereco>3</TipoEndereco>
        <CEP>65070565</CEP>
        <Logradouro>RUA B QUADRA</Logradouro>
        <Numero>12</Numero>
        <Bairro>VINHAIS II</Bairro>
        <NomeCidade>SAO LUIS</NomeCidade>
        <Cidade>SAO LUIS,MA,Brasil</Cidade>
        <UF>MA</UF>
        <Estado>Brasil,MA</Estado>
        <NomePais>Brasil</NomePais>
        <Pais>Brasil</Pais>
      </EnderecoPrincipal>
      <EnderecoCobranca>
        <TipoEndereco>1</TipoEndereco>
        <CEP>65070565</CEP>
        <Logradouro>RUA B QUADRA </Logradouro>
        <Numero>12</Numero>
        <Bairro>VINHAIS II</Bairro>
        <NomeCidade>SAO LUIS</NomeCidade>
        <Cidade>SAO LUIS,MA,Brasil</Cidade>
        <UF>MA</UF>
        <Estado>Brasil,MA</Estado>
        <NomePais>Brasil</NomePais>
        <Pais>Brasil</Pais>
      </EnderecoCobranca>
    </MSG0072>
  </CONTEUDO>
</MENSAGEM>
XMLSTRING;
libxml_use_internal_errors(true);

Postar("julio", "234", $xml_string);