<?php

/**
 * Arnaldo:
 * - Verificar se o arquivo bootstrap do drupal está sendo carregado corretamente no servidor
 * - Em Postar(), deve ser implementado o node_save
 */

include_once 'drupal_bootstrap.php';
require_once "nusoap/lib/nusoap.php";
libxml_use_internal_errors(true);


// Inicia o WSDL
$server = new soap_server();
$server->configureWSDL('ForbService', 'urn:ForbService');


// Funcao verificar
function Verificar() {
    return true;
}
$server->register('Verificar',
    array(),
    array('return' => 'xsd:boolen'),
    'urn:ForbService.Verificar',
    'urn:ForbService.Verificar',
    'rpc',
    'encoded',
    'Verificação de conexão'
    );



//Funcao Postar
/**
 * Funcao Postar
 * @param $usuario Opcional
 * @param $senha opcional
 * @param $requisicao XML
 * @return Array
 * <PostarResult>
 *  <result>true/false</result>
 *  <resposta>xml com resultado</resposta>
 * </PostarResult>
 */
function Postar($usuario, $senha, $requisicao) {
    $xml = simplexml_load_string($requisicao);
    
    if ($xml === false) {
        $sucesso = false;
        // Ocorreu um erro
        $mensagem = "";
         foreach(libxml_get_errors() as $error) {
             $mensagem .= urldecode(http_build_query((array) $error, '', ';')) . '\n';
        }
    } else {

        // TODO: Fazer parser de xml para $objeto
        // $node = node_save($objeto);
        //TODO: Fazer node_save
        $sucesso = true;
        $mensagem = 'Integração ocorrida com sucesso';
    }

    return array('Sucesso' => $sucesso, 'Mensagem' => $mensagem);
}


$server->wsdl->addComplexType(
    'PostarResult',
    'complextType',
    'struct',
    'sequence',
    '',
    array(
        'Sucesso' => array('name' => 'Sucesso', 'type' => 'xsd:boolean'),
        'Mensagem' => array('name' => 'Mensagem', 'type' => 'xsd:string'),
    )
);

$server->register('Postar',
    array('usuario' => 'xsd:String', 'senha' => 'xsd:String', 'requisicao' => 'xsd:String'),
    array('return' => 'tns:PostarResult'),
    'urn:ForbService.Postar',
    'urn:ForbService.Postar',
    'rpc',
    'encoded',
    'Função que realiza o parse do xml e faz a postagem'
);


$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA)
    ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);