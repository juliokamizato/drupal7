<?php
/**
 * @file
 * custom_product.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function custom_product_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'first_level_segments';
  $context->description = 'Contexts for segments taxonomy pages of root terms';
  $context->tag = 'products';
  $context->conditions = array(
    'cep_taxonomy_term_depth' => array(
      'values' => array(
        'top_level' => 'top_level',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'segments' => 'segments',
      ),
      'options' => array(
        'term_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-418ea0b1c6bfad28799fd56f70bf96f0' => array(
          'module' => 'views',
          'delta' => '418ea0b1c6bfad28799fd56f70bf96f0',
          'region' => 'content_bottom',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Contexts for segments taxonomy pages of root terms');
  t('products');
  $export['first_level_segments'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'lower_level_segments';
  $context->description = 'Contexts for segments taxonomy pages of terms with parent';
  $context->tag = 'products';
  $context->conditions = array(
    'cep_taxonomy_term_depth' => array(
      'values' => array(
        'lower_level' => 'lower_level',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'segments' => 'segments',
      ),
      'options' => array(
        'term_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-3f51f9455ea689f3679fbdf6ee14d7ef' => array(
          'module' => 'views',
          'delta' => '3f51f9455ea689f3679fbdf6ee14d7ef',
          'region' => 'content_bottom',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Contexts for segments taxonomy pages of terms with parent');
  t('products');
  $export['lower_level_segments'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_page';
  $context->description = '';
  $context->tag = 'products';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'product' => 'product',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'custom-banner_header' => array(
          'module' => 'custom',
          'delta' => 'banner_header',
          'region' => 'highlighted',
          'weight' => '-9',
        ),
        'easy_social-easy_social_block_1' => array(
          'module' => 'easy_social',
          'delta' => 'easy_social_block_1',
          'region' => 'content',
          'weight' => '-25',
        ),
        'custom_product-product_page_content' => array(
          'module' => 'custom_product',
          'delta' => 'product_page_content',
          'region' => 'content_bottom',
          'weight' => '-10',
        ),
        'custom_product-product_page_tabs' => array(
          'module' => 'custom_product',
          'delta' => 'product_page_tabs',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('products');
  $export['product_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'segments';
  $context->description = 'Contexts for segments taxonomy pages';
  $context->tag = 'products';
  $context->conditions = array(
    'taxonomy_term' => array(
      'values' => array(
        'segments' => 'segments',
      ),
      'options' => array(
        'term_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'custom-banner_header' => array(
          'module' => 'custom',
          'delta' => 'banner_header',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'custom_product-segment_search_style_selector' => array(
          'module' => 'custom_product',
          'delta' => 'segment_search_style_selector',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'menu_block-custom-product-segment-search' => array(
          'module' => 'menu_block',
          'delta' => 'custom-product-segment-search',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Contexts for segments taxonomy pages');
  t('products');
  $export['segments'] = $context;

  return $export;
}
