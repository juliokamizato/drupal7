/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.custom_product_list_filter = {
    attach: function(context) {

      $select = $('select.shl-select-list:last', context);
      $submit = $select.parents('form').find('.global-filter-set-default.form-submit');

      // Event listener which toggles the disabled state of the submit button
      // depending on whether the user has reached a product or not.
      // @todo: ABSTRACT THIS INTO THE CONFIG INTERFACE.
      $select.change(function(){
        var v = isValidProduct($(this).val());
        if(v === true) {
          $submit.removeAttr('disabled');
        }
        else {
          $submit.attr('disabled', true);
        }
      });
    }
  };

  $(document).ready(function() {
    setTimeout(function() {

      $('select.shl-select-list:last').change();
    }, 1000);
  });

  function isValidProduct(v) {
    if(v.length > 0) {
        v = v.split('|');
        if(v[0] === 'node') {
         return true;
      }
    }
    return false;
  }

})(jQuery, Drupal, this, this.document);

