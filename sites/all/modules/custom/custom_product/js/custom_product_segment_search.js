/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
//
(function ($, Drupal, window, document, undefined) {



  Drupal.behaviors.custom_product_segment_search = {
    attach: function(context) {
      // Initialize the plugins dependent on device type.
      if (Drupal.settings.intelbras.mobile) {
        //$('#block-menu-block-custom-product-segment-search .menu-block-wrapper', context).not('.custom_product-processed').addClass('custom_product-processed').hSlidingMenu();
      } else {
        //sticky menu
        $stickyBlock = $('.region-sidebar-first .block.first');
        $('.region-sidebar-first').waypoint(function(direction) {
          if (direction == 'down') {
            $stickyBlock.addClass('sticky');
            $stickyBlock.width($(this).width());
            $(this).height($stickyBlock.height()+95);
          } else {
            $stickyBlock.removeClass('sticky');
          }
        }, { offset: 0 });

        $('.endpage-wrapper').waypoint(function(direction) {
          if (direction == 'down') {
            $stickyBlock.addClass('end');
          } else {
            $stickyBlock.removeClass('end');
          }
        }, { offset: function() {
          bottomDistance = 130;
          size = $stickyBlock.height()+bottomDistance+95;
          return size;
        } });
      }
    }
  };

})(jQuery, Drupal, this, this.document);

