/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */
(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.custom_product_product_page = {
    attach: function(context) {
      // We want to sincronize with thumbs and add navigation for every image
      // gallery on the product page.
      $.each(Drupal.settings.flexslider_views_slideshow, function(index, el) {
        if (el.vss_id.indexOf('colors_and_images-product_image_gallery') > -1 && el.vss_id.indexOf('thumbs') === -1){
          index = el.vss_id.substring(el.vss_id.lastIndexOf("_") + 1);
          if(index == 'gallery'){
            index = 1;
          }
          $thumbs_slider = 'flexslider-' + index;

          // set the image galleries sync to the the thumbs
          el.sync = '#' + $thumbs_slider;
          // Copy the optionset
          Drupal.settings.flexslider.instances[$thumbs_slider] = 'product_image_gallery_thumbs_' + index;
          Drupal.settings.flexslider.optionsets['product_image_gallery_thumbs_' + index] = $.extend({}, Drupal.settings.flexslider.optionsets.product_image_gallery_thumbs);

          // Set the specific optionset asNavFor porperty to point to the respective image gallery
          Drupal.settings.flexslider.optionsets['product_image_gallery_thumbs_' + index].asNavFor = '.' + el.vss_id + '.flexslider';
        }
      });


      Drupal.settings.flexslider_views_slideshow['#flexslider_views_slideshow_main_products_db-product_video_gallery'].before =  function(slider) {
        if (slider.currentSlide != slider.animatingTo) {
          $player = $(slider.slides[slider.currentSlide]).find('iframe.media-youtube-player');
          Drupal.customYouTubeController.stopVideos($player);
        }
      };

      //image/video gallery switcher
      $('.node-product.view-mode-product_description .field-name-product-image-gallery-thumbs li', context).click(function() {
        $('.field-name-product-video-gallery').removeClass('not-hidden');
        $('.field-name-product-image-gallery').removeClass('hidden');
        Drupal.customYouTubeController.stopVideos();
      });
      $('.node-product.view-mode-product_description .field-name-product-video-gallery-thumbs li', context).click(function() {
        $('.field-name-product-video-gallery').addClass('not-hidden');
        $('.field-name-product-image-gallery').addClass('hidden');
      });

      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      //@REMOVE ME!!!! PRODUCT PAGE GAMBIARRA TO PUT THE DESCRIPTION OUTSIDE THE BOX
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      $productDescription = $('body.node-type-product .view-mode-product_description .field-collection-container')
      $('body.node-type-product .tab-content').eq(0).append($productDescription);

      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      //@REMOVE ME!!!! HIDE VIDEOS LABEL IF NO VIDEO
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if($('.field-name-product-video-gallery-thumbs').find('img').length == 0) {
        $('.field-name-product-video-gallery-thumbs').hide();
      }
    }
  };

  // Set low behavior weight so that our behavior gets called before the flexslider behavior.
  Drupal.behaviors.custom_product_product_page.weight = -1;

  Drupal.behaviors.custom_product_product_page_end = {
    attach: function(context) {
      // Make flexsliders adapt when click on node.
      $('.node-product.view-mode-product_description').click(function() { $(window).resize() });
      setInterval(function() { $(window).resize() }, 1000);

    }
  };

  // Set low behavior weight so that our behavior gets called before the flexslider behavior.
  Drupal.behaviors.custom_product_product_page_end.weight = 99;

})(jQuery, Drupal, this, this.document);

