/**
 * @file
 * A JavaScript file for Downloads pages.
 */
(function ($) {
  Drupal.behaviors.customProduct = {
    attach: function (context, settings) {
      $("#custom-product-downloads-filter-wrapper").once().tabs();
    }
  };
})(jQuery);
