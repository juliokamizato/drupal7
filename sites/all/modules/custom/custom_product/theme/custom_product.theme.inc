<?php

/**
 * This theme function renders the content of tabs for the product page.
 * @param  array $variables
 *   An associative array with the following keys:
 *   - title:  A string representing the title of the tab. It will be rendered in an <h2>.
 *   - $content: Either a Render array or markup to be included as the content of the tab.
 *
 * @return string            Rendered markup of the tab.
 */
function theme_product_page_tab($variables){
  $variables['content'] = is_array($variables['content']) ? drupal_render($variables['content']) : $variables['content'];
  $output = '<h2 class="tab-title">' . $variables['title'] . '</h2><div class="tab-content">' . $variables['content'] . '</div>';
  //@todo make this render array
  // $output = array(
  //   '#theme' => 'html_tag',
  //   '#attributes' => array(
  //     'class' => array('tab-title'),
  //   ),
  //   'content' => array(
  //     '#type' => 'container',
  //     '#attributes' => array(
  //       'class' => array('tab-content'),
  //     ),
  //   ),
  // );
  return $output;
}
