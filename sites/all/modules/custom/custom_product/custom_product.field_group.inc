<?php
/**
 * @file
 * custom_product.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function custom_product_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_partner_fields|node|product|form';
  $field_group->group_name = 'group_partner_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Partners information',
    'weight' => '15',
    'children' => array(
      0 => 'field_partner_instalation_ref',
      1 => 'field_partner_resale_ref',
      2 => 'field_partner_support_ref',
      3 => 'field_partner_reps_ref',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Partners information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_partner_fields|node|product|form'] = $field_group;

  return $export;
}
