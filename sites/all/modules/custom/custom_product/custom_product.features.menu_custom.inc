<?php
/**
 * @file
 * custom_product.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function custom_product_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: management.
  $menus['management'] = array(
    'menu_name' => 'management',
    'title' => 'Management',
    'description' => 'The <em>Management</em> menu contains links for administrative tasks.',
    'language' => 'und',
    'i18n_mode' => 0,
  );
  // Exported menu: menu-segments.
  $menus['menu-segments'] = array(
    'menu_name' => 'menu-segments',
    'title' => 'Segmentos',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Management');
  t('Segmentos');
  t('The <em>Management</em> menu contains links for administrative tasks.');


  return $menus;
}
