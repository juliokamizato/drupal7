<?php
/**
 * @file
 * Forms code for the Produto feature.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function custom_product_form_product_node_form_alter(&$form, &$form_state, $form_id) {
  // Make node title not required because in some cases we don't want to create
  // a benefit.
  $form['field_benefits_ref']['und']['form']['title']['#required'] = FALSE;

  // If this is a new translation, unset benefits reference because a new
  // benefit will be created with the translation.
  if (!empty($form_state['node']->translation_source) && $form_state['node']->translation_source && !$form_state['node']->nid && $form_state['build_info']['args'][0]->field_benefits_ref) {
    unset($form['#node']->field_benefits_ref);
    unset($form_state['build_info']['args'][0]->field_benefits_ref);
  }

  // Let's provide the default or current language as argument for our term
  // reference tree field.
  $form['field_segments'][$form['field_segments']['#language']]['#filter_view_args'] = $lang = custom_get_form_current_language($form, $form_state);

  // Get all segments for current language and put them in select field options
  // format.
  $results = views_get_view_result('segments', 'entityreference_segments_first_level', $lang);

  $options = array('_none' => t('- Selecione um valor -'));

  foreach ($results as $term) {
    $options[$term->tid] = $term->taxonomy_term_data_name;
  }

  // Let's get all field collection items in the field_product_descriptions and
  // insert the languge filtered options.
  $children = custom_get_field_multiple_children($form['field_product_descriptions']);

  foreach ($children as $child_key) {
    $child = &$form['field_product_descriptions'][$form['field_product_descriptions']['#language']][$child_key];
    $child['field_segment_ref'][$child['field_segment_ref']['#language']]['#options'] = $options;
  }

  // Wrap form elements which will be updted via ajax.
  custom_wrap_element($form['field_segments'], "edit-segments-wrapper");
  custom_wrap_element($form['field_product_descriptions'], "edit-field-product-descriptions-wrapper");

  // Add  ajax enabled field as a helper for the language field, since we
  // coudn't get ajax working on it.
  $form['lang_ajax'] = custom_ajaxfy_node_form_language_field('custom_product_language_ajax_callback');

  $weight = intval($form['field_int']['#weight']);

  $form['#after_build'][] = 'custom_product_node_form_after_build';

  // Prevents pathauto to generate the alias for the node.
  $form['path']['#access'] = FALSE;
  // Prevents the path to be set by the user.
  $form['path']['pathauto']['#default_value'] = 0;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function custom_product_form_entity_form_page_alter(&$form, &$form_state, $form_id) {
  // Custom submit function to sync the value of product relation with partner.
  $form['#submit'][] = 'custom_product_relation_partner_submit';

}

/**
 * Submit function for form entity_form_page.
 */
function custom_product_relation_partner_submit($form, $form_state) {

  $field_name  = reset($form_state['build_info']['field_names']);
  $nid  = $form_state['build_info']['entity_loaded']->nid;
  $target = array('target_id' => $nid);

  // Reset entity cache.
  $controller = entity_get_controller('node');
  $controller->resetCache(array(entity_id('node', $form_state['build_info']['entity_loaded'])));

  $product = node_load($nid);

  $partner_fields = array(
    'field_partner_instalation_ref' => 'field_products_instalation_ref',
    'field_partner_reps_ref' => 'field_products_reps_ref',
    'field_partner_resale_ref' => 'field_products_resale_ref',
    'field_partner_support_ref' => 'field_products_support_ref',
  );

  module_load_include('inc', 'custom', 'includes/helpers/custom_fields');

  foreach ($product->{$field_name}[LANGUAGE_NONE] as $key => $partner_nid) {

    $partner = node_load($partner_nid);
    $partner_field_relation = $partner->{$partner_fields[$field_name]}[LANGUAGE_NONE];

    if (in_array($target, $partner_field_relation) == FALSE) {
      $partner->{$partner_fields[$field_name]}[LANGUAGE_NONE][] = $target;
      custom_field_save('node', $partner);
    }
  }
}

/**
 * After build function.
 */
function custom_product_node_form_after_build($form) {
  $weight = intval($form['field_int']['#weight']);

  $form['translation']['#weight'] = $weight + 2;

  return $form;
}

/**
 * Implements hook_inline_entity_form_entity_form_alter().
 */
function custom_product_inline_entity_form_entity_form_alter(&$entity_form, &$form_state) {

  // If the parent node is a new translation, create a new entity, as a
  // translation of the child of the translation source of the parent entity,
  // along with the creation parent entity.
  if (!empty($entity_form['#parent_entity']->translation_source) && $entity_form['#parent_entity']->translation_source && (!$entity_form['#parent_entity']->nid && !$entity_form['#parent_entity']->id)) {

    module_load_include('inc', 'custom', 'custom.functions');

    // If the base entity is not a translation, make it a base translation.
    if ($entity_form['#entity']->tnid === '0') {
      $entity_form['#entity']->tnid = $entity_form['#entity']->nid;
      entity_save($entity_form['#entity_type'], $entity_form['#entity']);
    }

    // Get new object from  existing one.
    $entity_form['#entity'] = custom_get_entity_object_from_existing($entity_form['#entity_type'], $entity_form['#entity']);

    // Set attributes of the new object.
    $entity_form['#entity']->language = $entity_form['#parent_entity']->language;

  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function custom_product_form_global_filter_1_alter(&$form, &$form_state) {
  // Autocomplete for global filter title for products.
  $form['title']['#autocomplete_path'] = 'products/autocomplete/title';

  if (isset($form_state['input']['form_id']) &&
    $form_state['input']['form_id'] == 'global_filter_2') {
    $form['title']['#default_value'] = '';
  }

  // Custom bumit function to sync the value of this global filter with the one
  // which takes a categorical list.
  $form['submit']['#submit'][] = 'custom_product_sync_global_filters_submit';

  // Open the list filter tab by default.
  if (strpos($form['#action'], '#title-filter') === FALSE) {
    $form['#action'] .= '#title-filter';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function custom_product_form_global_filter_2_alter(&$form, &$form_state) {

  $form['nid']['#type'] = 'shl';
  // Custom bumit function to sync the value of this global filter with the one
  // which takes a categorical list.
  $form['submit']['#submit'][] = 'custom_product_sync_global_filters_submit';

  // JS to handle disabled state of the list filter submission button.
  $form['#attached']['js'][] = drupal_get_path('module', 'custom_product') . '/js/custom_product_list_filter.js';
}

/**
 * Submit function for sync global filters.
 */
function custom_product_sync_global_filters_submit($form, &$form_state) {

  if ($form_state['triggering_element']['#type'] == 'submit') {

    // If the form was submitted through the submission button, then we have a
    // product certainly for the list filter and maybe for the title filter.
    // Either way, we set the active filter session variable so that we can use
    // it our views pre build to handle arguments and clear the global filter
    // which is not being used.
    $gf_submitted = reset($form_state['global_filters']);
    $gf_sync = ($gf_submitted['name'] === 'title') ? 'nid' : 'title';

    $val = ($gf_sync == 'nid') ? array('') : '';

    global_filter_set_on_session($gf_sync, $val);
    $_SESSION['active_global_filter'] = $gf_submitted['name'];
  }
}
