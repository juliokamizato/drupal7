<?php
/**
 * @file
 * Callback code for the Product feature.
 */

/**
 * Callback funtion which returns the field to be updated on language change.
 * @param  Array $form
 *         The current form being manipulated
 * @param  Array $form_state
 *         The state of the form
 * @return Array             Render Array for the appropriate form element
 */
function custom_product_language_ajax_callback($form, $form_state){
  $form = drupal_rebuild_form('product_node_form', $form_state, $form);
  $commands = array();
  $commands[] = ajax_command_replace("#edit-segments-wrapper", drupal_render($form["field_segments"]));
  $commands[] = ajax_command_replace("#edit-field-product-descriptions-wrapper", drupal_render($form["field_product_descriptions"]));
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Autocomplete callback function which returns all products
 * whose titles contains the string given.
 * @param  string $string A string to match with.
 * @return string         JSON array containing the matching products.
 */
function custom_product_autocomplete_callback($string = '') {
  global $language;

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->propertyCondition('type', 'product');
  $query->propertyCondition('language', array($language->language, LANGUAGE_NONE));
  $query->propertyCondition('title', $string, 'CONTAINS');
  $query->range(0, 10);
  $results = $query->execute();

  $matches = array();

  if(!empty($results['node'])) {
    $matches = entity_load('node', array_keys($results['node']));

    foreach ($matches as $nid => $node) {
     $matches[$node->title] = $node->title;
     unset($matches[$nid]);
   }
 }

 drupal_json_output($matches);
}
