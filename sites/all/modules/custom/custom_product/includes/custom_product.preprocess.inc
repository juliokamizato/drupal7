<?php
/**
 * @file
 * Preprocesses code for the Produto feature.
 */

/**
 * Implements hook_preprocess_template().
 */
function custom_product_preprocess_segment_search_style_selector(&$vars) {
  drupal_add_js(drupal_get_path('module', 'custom_product') . '/js/custom_product_segment_search.js');
}

/**
 * Implements hook_preprocess_views_view_fields().
 */
function custom_product_preprocess_views_view_fields(&$vars) {
  module_load_include('inc', 'custom', 'custom.functions');

  if ($vars['view']->name == 'products' && $vars['view']->current_display == 'segment_search_mobile') {
    // We need to present the product description depending on context,
    // so we load the node and get all descriptions.
    $node = entity_load('node', array($vars['row']->entity));
    if (!empty($node)) {
      $node = array_shift($node);
    }
    $wrapper = entity_metadata_wrapper('node', $node);
    $descs = $wrapper->field_product_descriptions->value();

    if (!empty($descs)) {

      // Load the menu object.
      $menu_obj = menu_get_object('taxonomy_term', 2);

      // Match the description with the taxonomy term of the page that
      // made the request.
      if ($desc = custom_product_get_description($descs, $menu_obj->tid)) {
        $vars['fields']['field_product_descriptions']->content = drupal_render($desc);
        $vars['fields']['field_product_descriptions']->element_type = 'div';
      }
      else {
        unset($vars['fields']['field_product_descriptions']);
      }
    }
  }

  if ($vars['view']->name == 'products' && ($vars['view']->current_display == 'block_product_release') || ($vars['view']->current_display == 'block_product_release_mobile')) {

    // I need get the first field_product_descriptions_item_id.
    $item_id = explode(',', $vars['fields']['field_product_descriptions_item_id']->content);

    $item_id = $item_id[0];
    $field_collection = field_collection_item_load($item_id);

    $desc = !empty($field_collection) ? entity_metadata_wrapper('field_collection_item', $field_collection) : NULL;

    // I need to clear this content to not show him.
    $vars['fields']['field_product_descriptions_item_id']->wrapper_prefix = NULL;
    $vars['fields']['field_product_descriptions_item_id']->wrapper_suffix = NULL;
    $vars['fields']['field_product_descriptions_item_id']->content = NULL;

    // I need to change this content by field collection description content.
    $render = $desc ? $desc->view('description_summary_only') : array();
    $vars['fields']['field_product_descriptions']->content = drupal_render($render);
  }
}

/**
 * Implements hook_product_preprocess_node().
 */
function custom_product_preprocess_node(&$vars) {
  if ($vars['type'] == 'product') {
    if ($vars['view_mode'] == 'technical_specification') {
      unset($vars['content']['links']);
      $vars['page'] = TRUE;
    }

    // @todo add subtitle field elegant form.
    if ($vars['view_mode'] == 'mosaic') {
      $vars['content']['field_subtitle']['#markup'] = '<div class="field field-name-field-subtitle field-type-text field-label-hidden"></div>';
    }

    // This view_mode, i need to know that field description to show.
    if ($vars['view_mode'] == 'product_description') {

      $uri_exploded = custom_get_exploded_url();

      // The current path is an internal path e.g. node/123.
      if (in_array('node', $uri_exploded)) {
        return;
      }

      // I cannot to use this function taxonomy_get_term_by_name() because her
      // needs the exactly string name...
      if (count($uri_exploded) > 1) {

        $node_wrapper = entity_metadata_wrapper('node', $vars['node']);

        foreach ($node_wrapper->field_product_descriptions->value() as $key => $field_collection) {

          if ($collection_wrapper = entity_metadata_wrapper('field_collection_item', $field_collection)) {

            if (empty($collection_wrapper->value()->field_segment_ref)) {
              break;
            }

            $tid = $collection_wrapper->field_segment_ref->value()->tid;

            if (!empty($tid)) {
              $terms[] = taxonomy_term_load($tid);
            }
          }
        }

        if (!empty($terms)) {

          next($uri_exploded);
          // Now I need to know that segment is equal an url.
          $term_wrapper = FALSE;
          foreach ($terms as $key => $term) {
            if (custom_pathauto_cleanstring($term->name) == current($uri_exploded)) {
              $term_wrapper = entity_metadata_wrapper('taxonomy', $term);
              break;
            }
          }

          // Shows the first description value if there is no
          // segment on the URL.
          if (empty($term_wrapper)) {
            foreach ($node_wrapper->field_product_descriptions as $key => $segment) {
              if ($key != 0) {
                unset($vars['content']['field_product_descriptions'][$key]);
              }
            }
            return;
          }

          $only_first = 0;
          foreach ($node_wrapper->field_product_descriptions as $key => $segment) {

            $segment_ref = $segment->field_segment_ref->value();

            if (!empty($segment_ref) &&
                !empty($term_wrapper) &&
                !empty($term_wrapper->value()->name) &&
                $node_wrapper->field_product_descriptions[$key]->field_segment_ref->value()->name != $term_wrapper->value()->name) {
              unset($vars['content']['field_product_descriptions'][$key]);
            }

            // @todo remove this algoritm after create a validate in
            // the product create form in the field collection product.
            if ($only_first) {
              unset($vars['content']['field_product_descriptions'][$key]);
            }
            // Only description with same segment, Only first I get.
            if (!empty($segment_ref) &&
                !empty($term_wrapper) &&
                !empty($term_wrapper->value()->name) &&
                $node_wrapper->field_product_descriptions[$key]->field_segment_ref->value()->name == $term_wrapper->value()->name) {

              if (!$only_first) {
                $only_first = 1;
              }
            }
          }
        }
      }
    }

    // Script for location search button lock.
    $path = drupal_get_path('module', 'custom') . '/js/support_search.js';
    drupal_add_js($path);
  }

  if ($vars['type'] == 'benefit') {
    if ($vars['view_mode'] == 'teaser') {
      $vars['content']['body']['#title'] = t('Main Benefits');
    }
  }
}

/**
 * Implements hook_preprocess_page().
 */
function custom_product_preprocess_page(&$vars) {
  if (isset($vars['node']) && $vars['node']->type == 'product') {
    $node = $vars['node'];

    $wrapper = entity_metadata_wrapper('node', $node);
    $uri_exploded = custom_get_exploded_url();

    foreach ($wrapper->field_segments->value() as $key => $segment) {

      if (isset($segment->name) && custom_pathauto_cleanstring($segment->name) == $uri_exploded[1]) {
        $vars['title'] = $segment->name;
        break;
      }
      else {
        $vars['title'] = NULL;
      }
    }

    module_load_include('inc', 'custom_breadcrumbs_taxonomy');
    $segments = $wrapper->field_segments->value();
    $breadcrumb = drupal_get_breadcrumb();

    // Create breadcrumb on the fly.
    foreach ($uri_exploded as $key => $uri) {
      foreach ($segments as $key => $segment) {
        if (isset($segment) && $uri == custom_pathauto_cleanstring($segment->name)) {
          $segment_name = $segment->name;
          $breadcrumb[] = l(t($segment_name), 'taxonomy/term/' . $segment->tid);
          break;
        }
      }
    }

    drupal_set_breadcrumb($breadcrumb);

    $vars['subtitle'] = t('Products');
  }
}
