<?php
/**
 * @file
 * Callback code for the Product batch actions.
 */

/**
 * Batch init Callback
 */
function custom_product_url_batch_init() {
  global $language;

  $batch = array(
    'title' => t('Updating Products URLs ...'),
    'operations' => array(),
    'init_message' => t("Product's URL update is starting."),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('An error occurred during processing'),
    'finished' => 'custom_product_url_batch_finished',
    'progressive' => FALSE,
    'file' => drupal_get_path('module', 'custom_product') . '/includes/custom_product.batch.inc',
  );

  $query = new EntityFieldQuery();
  // @TODO: Filtering just by pt-br nodes to prevent the EN and ES path creation
  // issue in the batch proccess.
  $result = $query
    ->entityCondition('entity_type', 'node')
    ->propertyCondition('status', 1)
    ->propertyCondition('type', 'product')
    ->propertyCondition('language', $language->language)
    ->execute();

  $nodes = $result['node'];

  foreach ($nodes as $node) {
    $batch['operations'][] = array('custom_product_url_batch_worker', array($node->nid));
  }

  batch_set($batch);
  // If this function was called from a form submit handler, stop here,
  // FAPI will handle calling batch_process().
  // If not called from a submit handler, add the following,
  // noting the url the user should be sent to once the batch
  // is finished.
  // IMPORTANT:
  // If you set a blank parameter, the batch_process() will cause an infinite loop
  batch_process('/admin/content');
}

/**
 * Batch Operation Callback
 */
function custom_product_url_batch_worker($node_nid, &$context) {

  $node = node_load($node_nid);
  _custom_product_node_create_alternative_url($node);

  $context['results']['processed']++;
  $context['message'] = t('Updating alternative path for the product %node', array('%node' => $node->title));
}

/**
 * Batch 'finished' callback
 */
function custom_product_url_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural($results['processed'], 'One node processed.', '@count nodes processed.');
    $status = 'status';
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    $status = 'error';
  }

  drupal_set_message($message, $status);
}
