<?php

/**
 * @file
 * Utils and Private Functions
 */

/**
 * Helper function to save the product URLs based on its segments.
 */
function _custom_product_node_create_alternative_url($node) {

  module_load_include('inc', 'custom_breadcrumbs_taxonomy');

  $alt_path = array();

  // Gets the last child of each segment.
  $last_child = _custom_product_get_segments($node);

  foreach ($last_child as $key => $segments) {
    foreach ($segments as $segment => $term_tid) {

      $alt_path[$key][$segment] = custom_breadcrumbs_taxonomy_generate_breadcrumb($term_tid);

      // Removes the link tag.
      $alt_path[$key][$segment] = array_map('strip_tags', $alt_path[$key][$segment]);

      // Adds the node title to the trail.
      $alt_path[$key][$segment][] = $node->title;

      // Urlfy the crumbs.
      $alt_path[$key][$segment] = array_map('custom_pathauto_cleanstring', $alt_path[$key][$segment]);

      // Removes the Home crumb of the trail.
      if (strtolower($alt_path[$key][$segment][0]) == 'home') {
        unset($alt_path[$key][$segment][0]);
      }

      // Build the final path.
      $alt_path[$key][$segment] = implode('/', $alt_path[$key][$segment]);
    }
  }
  $current_paths = array();
  $current_paths = custom_path_load('node/' . $node->nid);

  // Removes paths that doens't match to the actual segments.
  foreach ($current_paths as $path) {

    if (!in_array($path->alias, $alt_path)) {
      path_delete($path->pid);
    }
  }

  // Check for paths already created and remove it from the list.
  foreach ($alt_path as $segment => $url) {
    $path = path_load(array('alias' => $url));
    if (isset($path['pid'])) {
      unset($alt_path[$segment]);
    }
  }

  // Creates the paths if needed.
  if (!empty($alt_path)) {
    foreach ($alt_path as $path) {
      // Saves the alternative paths if the product belongs to more than one
      // segment.
      custom_save_alternative_path($node, current($path));
    }
  }
}

/**
 * Get the product description for the segment given.
 *
 * @param array $descriptions
 *   An array of field_collection_items
 * @param int $segment_tid
 *   The term id of the segment of the description
 * @param string $view_mode
 *   View mode of the field_product_descriptions bundle
 *
 * @return mixed
 *   Returns a Render Array of the first matching description, FALSE if none
 *   matched.
 */
function custom_product_get_description($descriptions, $segment_tid, $view_mode = 'description_summary_only') {
  global $language;
  foreach ($descriptions as $delta => $desc) {
    $desc = entity_metadata_wrapper('field_collection_item', $desc);
    $segment = $desc->field_segment_ref->value();
    if (!empty($segment) && $segment->tid == $segment_tid) {
      return $desc->view($view_mode);
    }
  }
  return FALSE;
}

/**
 * Helper function to retrive the last child of the produc segments.
 *
 * @param object $node
 *   The node to get the segments.
 *
 * @return array
 *   An keyed array with parent => last child.
 */
function _custom_product_get_last_segment_child($node) {

  $node_wrapper = entity_metadata_wrapper('node', $node);

  $terms = $node_wrapper->field_segments->value();

  $last_child = array();

  foreach ($terms as $key => $term) {
    if (!is_object($term)) {
      continue;
    }
    // Builds an array of segment name => last child term tid.
    $parents = taxonomy_get_parents_all($term->tid);
    $parent_name = strtolower(end($parents)->name);

    if (!isset($terms[$key + 1])) {
      $last_child[][$parent_name] = $term->tid;
    }
    elseif (custom_term_is_last_child($term, $terms[$key + 1])) {
      $last_child[][$parent_name] = $term->tid;
    }
  }
  return $last_child;
}

/**
 * Helper function to retrive the product segments.
 *
 * @param object $node
 *   The node to get the segments.
 *
 * @return array
 *   An keyed array with parent => last child.
 */
function _custom_product_get_segments($node) {
  $node_wrapper = entity_metadata_wrapper('node', $node);

  $terms = $node_wrapper->field_segments->value();

  $segments = array();

  foreach ($terms as $key => $term) {
    $parents = taxonomy_get_parents_all($term->tid);
    $parent_name = strtolower(end($parents)->name);

    $segments[][$parent_name] = $term->tid;

  }
  return $segments;
}

/**
 * Get products by legacy CRM ids.
 *
 * @param mixed $crm_ids
 *   Legacy crm id (array or int).
 *
 * @return array
 *   List of entity ids of products.
 */
function custom_product_get_ids_by_crm_id($crm_ids, $language = NULL) {

  $result = array();
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'product');

  if ($language) {
    $query->propertyCondition('language', $language);
  }

  $query->fieldCondition('field_legacy_crm_id', 'value', (array) $crm_ids, 'in');

  $result = $query->execute();

  return !empty($result['node']) ? $result['node'] : array();
}
