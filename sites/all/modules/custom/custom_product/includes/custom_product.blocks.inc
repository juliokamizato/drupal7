<?php
/**
 * @file
 * Block code for the Produto feature.
 */

/**
 * Implments hook_block_info().
 */
function  custom_product_block_info(){

  $blocks['segment_search_style_selector'] = array(
    'info' => t('Segment Search Style Selector'),
  );

  $blocks['product_page_content'] = array(
    'info' => t('Product Page Content'),
  );

  $blocks['product_page_tabs'] = array(
    'info' => t('Product Page Tabs'),
  );

  // Wrapper block for the Downloads tab
  $blocks['download_filter_tabs'] = array(
    'info' => t('Product Download filter tabs'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function custom_product_block_view($delta = ''){

  global $language;


  $block = array();
  switch ($delta) {

    case 'product_page_content':

      $block['subject'] = NULL;

      $node = menu_get_object();
      global $language;

      $block['content'] = array(
        '#theme' => 'item_list',
        '#attributes' => array(
          'class' => array('product-page-content')
        ),
        '#items' => array(
          'description' => custom_product_page_tab_view(
            'Description',
            entity_view(
              'node',
              array($node),
              $view_mode = 'product_description',
              $language->language,
              TRUE
            )
          ),
          'specs' => custom_product_page_tab_view(
            'Technical Specifications',
            entity_view(
              'node',
              array($node),
              $view_mode = 'technical_specification',
              $language->language,
              TRUE
            )
          ),
          'step_by_step' => custom_product_page_tab_view(
            'Step by step configuration',
            custom_views_render(
              'download_db',
              'step_by_step',
              array($node->nid)
            )
          ),
          'downloads' => custom_product_page_tab_view(
            'Downloads',
            custom_views_render(
              'download_db',
              'product_downloads',
              array($node->nid)
            )
          ),
          'faq' => custom_product_page_tab_view(
            'FAQ',
            custom_views_render(
              'q_and_a_categories',
              'categories_with_q_and_a'
            )
          ),
        ),
      );

      if ($language->language == 'pt-br') {
        $block['content']['#items']['buy_and_install'] = custom_product_page_tab_view(
          'Where to buy and who installs?',
          custom_views_render(
            'partners_solr',
            'product_reseller_installer',
            array($node->nid)
          )
        );

        $block['content']['#items']['support'] = custom_product_page_tab_view(
          'Technical Support',
          custom_views_render(
            'partners_solr',
            'product_technical_support'
          )
        );
      }

      break;

    case 'product_page_tabs':

      $block['subject'] = NULL;
      $block['content'] = array(
        '#theme' => 'item_list',
        '#type' => 'ul',
        '#attributes' => array('class' => 'product-page-tabs'),
        '#items' => array(
          t('Description'),
          t('Technical Specifications'),
          t('Step by step configuration'),
          t('Downloads'),
          t('FAQ'),
        ),
      );

      if ($language->language == 'pt-br') {
        $block['content']['#items'][] = t('Where to buy and who installs?');
        $block['content']['#items'][] = t('Technical Support');
      }
      break;

    case 'download_filter_tabs':

      drupal_add_library('system', 'ui.tabs');

      $block['subject'] = NULL;
      $block['content'] = array();

      $first = block_load('global_filter', 'global_filter_1');
      $second = block_load('global_filter', 'global_filter_2');

      $blocks = _block_render_blocks(array($first, $second));
      $renderable_blocks = _block_get_renderable_array($blocks);

      $block_content = array(
        // Tabs links
        'tabs' => array(
          '#theme' => 'item_list',
          '#type' => 'ul',
          '#attributes' => array('class' => 'downloads-filter-tabs'),
          '#items' => array(
            l(t('Filter by class'), '#', array(
              'external' => TRUE,
              'fragment' => 'class-filter',
            )),
            l(t('Filter by title'), '#', array(
              'external' => TRUE,
              'fragment' => 'title-filter',
            )),
          ),
        ),
        // Tabs item
        'tab1' => array(
          '#type' => 'container',
          '#attributes' => array(
            'id' => 'class-filter',
          ),
          'tab_content' => $renderable_blocks['global_filter_global_filter_2'],
        ),
        // Tabs item
        'tab2' => array(
          '#type' => 'container',
          '#attributes' => array(
            'id' => 'title-filter',
          ),
          'tab_content' => $renderable_blocks['global_filter_global_filter_1'],
        ),
      );

      $block['content'] = array(
        'tabs' => array(
          '#type' => 'container',
          '#attributes' => array(
            'id' => 'custom-product-downloads-filter-wrapper',
          ),
          'content' => $block_content,
        ),
      );

      drupal_add_js(drupal_get_path('module', 'custom_product') . '/js/custom_product_downloads_page.js', array(
        'type' => 'file',
        'scope' => 'footer',
      ));
      break;
  }

  return $block;
}


/**
 * Return a list of configurations for menu blocks.
 *
 * Modules that want to have menu block configurations exported to code should
 * provide them using this hook.
 *
 * @see menu_tree_build() for a description of the config array.
 */
function custom_product_menu_block_blocks() {
  return array(
    // The array key is the block id used by menu block.
    'custom-product-segment-search' => array(
      // Use the array keys/values described in menu_tree_build().
      'menu_name'   => 'menu-segments',
      'parent_mlid' => 0,
      'title_link'  => FALSE,
      'admin_title' => 'Segment Search Menu',
      'level'       => 2,
      'follow'      => 0,
      'depth'       => 0,
    ),
    // The array key is the block id used by menu block.
    'custom-product-first-segments' => array(
      // Use the array keys/values described in menu_tree_build().
      'menu_name'   => 'menu-segments',
      'parent_mlid' => 0,
      'title_link'  => FALSE,
      'admin_title' => 'First Level Segments',
      'level'       => 0,
      'follow'      => 0,
      'depth'       => 1,
      'expanded'    => TRUE,
      'sort'        => FALSE,
    ),
  );
}
