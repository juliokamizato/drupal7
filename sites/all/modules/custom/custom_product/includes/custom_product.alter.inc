<?php

/**
 * Implements hook_views_ajax_data_alter.
 */
function custom_product_views_ajax_data_alter(&$commands, $view) {
  //if is the segment views for mobile calls an js functions to handle clicks after paging
  if ($view->name == 'segments' && $view->current_display == 'segment_search_mobile_segments'){
    $commands[] = array('command' => 'segmentsAjaxCallback', 'foo' => 'bar');
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function custom_product_menu_local_tasks_alter(&$data, $router_item, $root_path) {

  $object = menu_get_object();

  // I need to remove this tabs when be accessing benefit's node
  if ((isset($object->type)) && ($object->type == 'benefit')) {

    unset($data['tabs']);
  }
}

/**
 * Implements hook_block_view_alter();
 */
function custom_product_block_view_alter(&$data, $block){

  $global_filter_delta = ($block->delta == 'global_filter_1' || $block->delta == 'global_filter_2');

  if ($block->module == 'global_filter' && $global_filter_delta) {
    // Clean dirty title that is coming from idk where.
    $data['subject'] = NULL;
  }

  if ($block->module == 'menu_block' && $block->delta == 'custom-product-segment-search') {

    // Remove the block title because we don't need it.
    $data['subject'] = NULL;

    // Adds custom path for segment search menu items
    $items = element_children($data['content']['#content']);

    foreach ($items as $mlid) {
      $item = _custom_product_alter_segment_search_menu_item($data['content']['#content'][$mlid]);

      //remove the items which do not have product within their categories.
      if(empty($item))
        unset($data['content']['#content'][$mlid]);
      else
        $data['content']['#content'][$mlid] = $item;
    }
  }

}

/**
 * This helper function receives a menu item and return false for items which
 * do not have any products registered with it's respective category.
 * @param  array $element Render array representing the menu item.
 * @return mixed          The altered menu element, FALSE if the element does
 *                              not have any products.
 */
function _custom_product_alter_segment_search_menu_item($element) {
  $tid = custom_get_tid_from_path($element['#href']);

  // Hide the item in case it does not have any products.
  $products = views_get_view_result('products_by_segment', 'default', $tid);

  if (empty($products)) {
    return false;
  }

  $below = element_children($element['#below']);

  if (!empty($below)) {
    foreach ($below as $mlid) {
      // Recurse for sub menu items.
      $element['#below'][$mlid] = _custom_product_alter_segment_search_menu_item($element['#below'][$mlid]);
      if(empty($element['#below'][$mlid]))
        unset($element['#below'][$mlid]);
    }
    $element['#attributes']['class'][] = 'has-children';
  }
  return $element;
}

/**
 * Implements hook_blockreference_potential_references2_alter().
 */
function custom_product_blockreference_potential_references2_alter(&$blocks, $context) {

  $theme = variable_get('theme_default', 'bartik');

  // remove menu segments from the list because it cannot be used as a submenu
  $result = db_select('block', 'b')
  ->fields('b', array('bid'))
  ->condition('module', 'menu','=')
  ->condition('delta', 'menu-segments','=')
  ->condition('theme', $theme,'=')
  ->execute()
  ->fetchAssoc();

  unset($blocks[$result['bid']]);

  // remove all blcok provided by the menu_block module with the exception of our
  // custom-product-first-segments, which replaces the standard menu-segments.
  $result = db_select('block', 'b')
  ->fields('b', array('bid'))
  ->condition('module', 'menu_block','=')
  ->condition('delta', 'custom-product-first-segments','!=')
  ->condition('theme', $theme,'=')
  ->execute();

  while ($block = $result->fetchAssoc()) {
    unset($blocks[$block['bid']]);
  }

}

/**
 * Implements hook_entity_view_alter().
 */
function custom_product_entity_view_alter(&$build, $type) {

  if ($type == 'node' && !empty($build['#bundle']) && $build['#bundle'] == 'product') {
    // Add youtube frame interface library and description behavior to product's
    // product_description view mode
    if ($build['#view_mode'] == 'product_description') {
      $build['#attached']['js'][] = drupal_get_path('module', 'custom_product').'/js/custom_product_product_description.js';
      $build['#attached']['libraries_load'][] = array('youtube_frame_interface');
    }

    // Addes a link wrapper for the product's mosaic view mode
    if ($build['#view_mode'] == 'mosaic') {
      $url = custom_get_exploded_url();

      // @todo
      // This query get the correct url for this node based url context
      $query = db_select('url_alias', 'a');
      $query->fields('a', array('alias'));
      $query->condition('a.language', $build['#node']->language);
      $query->condition('a.source', 'node/' . $build['#node']->nid);
      $query->condition('a.alias', $url[1] . '%', 'LIKE');
      $url_alias = $query->execute()->fetchField();

      $old_build = $build;
      $nid = $old_build['#node']->nid;
      $language = $old_build['#node']->language;
      $build = array();
      $build['#node'] = $old_build['#node'];
      $build['#contextual_links'] = $old_build['#contextual_links'];
      // Removes contextual links because they break the anchor wrapper.
      unset($old_build['#contextual_links']);

      $build['#theme'] = 'link';
      $build['#text'] = drupal_render($old_build);
      $build['#path'] = url($url_alias);
      $build['#options'] = array(
        'attributes' => array(),
        'html' => TRUE,
      );
    }
  }
}
