<?php
/**
 * @file
 * custom_product.default_cer_presets.inc
 */

/**
 * Implements hook_default_cer().
 */
function custom_product_default_cer() {
  $export = array();

  $cnr_obj = new stdClass();
  $cnr_obj->disabled = FALSE; /* Edit this to true to make a default cnr_obj disabled initially */
  $cnr_obj->api_version = 1;
  $cnr_obj->entity_types_content_fields = 'node*partner*field_products_instalation_ref*node*product*field_partner_instalation_ref';
  $cnr_obj->enabled = 1;
  $export['node*partner*field_products_instalation_ref*node*product*field_partner_instalation_ref'] = $cnr_obj;

  $cnr_obj = new stdClass();
  $cnr_obj->disabled = FALSE; /* Edit this to true to make a default cnr_obj disabled initially */
  $cnr_obj->api_version = 1;
  $cnr_obj->entity_types_content_fields = 'node*partner*field_products_reps_ref*node*product*field_partner_reps_ref';
  $cnr_obj->enabled = 1;
  $export['node*partner*field_products_reps_ref*node*product*field_partner_reps_ref'] = $cnr_obj;

  $cnr_obj = new stdClass();
  $cnr_obj->disabled = FALSE; /* Edit this to true to make a default cnr_obj disabled initially */
  $cnr_obj->api_version = 1;
  $cnr_obj->entity_types_content_fields = 'node*partner*field_products_resale_ref*node*product*field_partner_resale_ref';
  $cnr_obj->enabled = 1;
  $export['node*partner*field_products_resale_ref*node*product*field_partner_resale_ref'] = $cnr_obj;

  $cnr_obj = new stdClass();
  $cnr_obj->disabled = FALSE; /* Edit this to true to make a default cnr_obj disabled initially */
  $cnr_obj->api_version = 1;
  $cnr_obj->entity_types_content_fields = 'node*partner*field_products_support_ref*node*product*field_partner_support_ref';
  $cnr_obj->enabled = 1;
  $export['node*partner*field_products_support_ref*node*product*field_partner_support_ref'] = $cnr_obj;

  return $export;
}
