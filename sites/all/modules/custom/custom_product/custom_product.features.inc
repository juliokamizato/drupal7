<?php
/**
 * @file
 * custom_product.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function custom_product_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "cer" && $api == "default_cer_presets") {
    return array("version" => "1");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function custom_product_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function custom_product_image_default_styles() {
  $styles = array();

  // Exported image style: product_benefit_small.
  $styles['product_benefit_small'] = array(
    'name' => 'product_benefit_small',
    'effects' => array(
      30 => array(
        'label' => 'Escala',
        'help' => 'Escalar a imagem manterá suas proporções originais. Se apenas uma das dimensões for dada, a outra será automaticamente calculada.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 150,
          'height' => 95,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'product_benefit_small',
  );

  // Exported image style: product_gallery_large.
  $styles['product_gallery_large'] = array(
    'name' => 'product_gallery_large',
    'effects' => array(
      32 => array(
        'label' => 'Escala',
        'help' => 'Escalar a imagem manterá suas proporções originais. Se apenas uma das dimensões for dada, a outra será automaticamente calculada.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 380,
          'height' => 370,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'product_gallery_large',
  );

  // Exported image style: product_gallery_thumb.
  $styles['product_gallery_thumb'] = array(
    'name' => 'product_gallery_thumb',
    'effects' => array(
      34 => array(
        'label' => 'Escala',
        'help' => 'Escalar a imagem manterá suas proporções originais. Se apenas uma das dimensões for dada, a outra será automaticamente calculada.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 50,
          'height' => 45,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'product_gallery_thumb',
  );

  // Exported image style: product_release_mobile_small.
  $styles['product_release_mobile_small'] = array(
    'name' => 'product_release_mobile_small',
    'effects' => array(
      36 => array(
        'label' => 'Escala',
        'help' => 'Escalar a imagem manterá suas proporções originais. Se apenas uma das dimensões for dada, a outra será automaticamente calculada.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 220,
          'height' => 195,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'product_release_mobile_small',
  );

  // Exported image style: product_release_small.
  $styles['product_release_small'] = array(
    'name' => 'product_release_small',
    'effects' => array(
      42 => array(
        'label' => 'Escala',
        'help' => 'Escalar a imagem manterá suas proporções originais. Se apenas uma das dimensões for dada, a outra será automaticamente calculada.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 253,
          'height' => 253,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'product_release_small',
  );

  // Exported image style: segment_page_banner.
  $styles['segment_page_banner'] = array(
    'name' => 'segment_page_banner',
    'effects' => array(
      37 => array(
        'label' => 'Escala',
        'help' => 'Escalar a imagem manterá suas proporções originais. Se apenas uma das dimensões for dada, a outra será automaticamente calculada.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 715,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'segment_page_banner',
  );

  // Exported image style: segment_search_large.
  $styles['segment_search_large'] = array(
    'name' => 'segment_search_large',
    'effects' => array(
      39 => array(
        'label' => 'Escala',
        'help' => 'Escalar a imagem manterá suas proporções originais. Se apenas uma das dimensões for dada, a outra será automaticamente calculada.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 210,
          'height' => 200,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'segment_search_large',
  );

  // Exported image style: segment_search_mobile_large.
  $styles['segment_search_mobile_large'] = array(
    'name' => 'segment_search_mobile_large',
    'effects' => array(
      43 => array(
        'label' => 'Escala',
        'help' => 'Escalar a imagem manterá suas proporções originais. Se apenas uma das dimensões for dada, a outra será automaticamente calculada.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 185,
          'height' => 185,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'segment_search_mobile_large',
  );

  // Exported image style: segment_search_mosaic_large.
  $styles['segment_search_mosaic_large'] = array(
    'name' => 'segment_search_mosaic_large',
    'effects' => array(
      1 => array(
        'label' => 'Escala',
        'help' => 'Escalar a imagem manterá suas proporções originais. Se apenas uma das dimensões for dada, a outra será automaticamente calculada.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 410,
          'height' => 410,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'segment_search_mosaic_large',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function custom_product_node_info() {
  $items = array(
    'benefit' => array(
      'name' => t('Benefício'),
      'base' => 'node_content',
      'description' => t('Benefício do produto'),
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
    'product' => array(
      'name' => t('Produto'),
      'base' => 'node_content',
      'description' => t('Registro do produto'),
      'has_title' => '1',
      'title_label' => t('Nome do produto'),
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function custom_product_default_search_api_index() {
  $items = array();
  $items['product'] = entity_import('search_api_index', '{
    "name" : "products",
    "machine_name" : "product",
    "description" : "Indexes all content of the Product Content Type",
    "server" : "intelbras",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "100",
      "fields" : {
        "nid" : { "type" : "integer" },
        "vid" : { "type" : "integer" },
        "type" : { "type" : "string" },
        "title" : { "type" : "text", "boost" : "5.0", "real_type" : "text_ngram" },
        "language" : { "type" : "string" },
        "url" : { "type" : "uri" },
        "status" : { "type" : "integer" },
        "promote" : { "type" : "boolean" },
        "sticky" : { "type" : "boolean" },
        "created" : { "type" : "date" },
        "changed" : { "type" : "date" },
        "author" : { "type" : "integer", "entity_type" : "user" },
        "source" : { "type" : "integer", "entity_type" : "node" },
        "comment_count" : { "type" : "integer" },
        "field_subtitle" : { "type" : "text", "boost" : "5.0", "real_type" : "text_ngram" },
        "field_segments" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_release" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_partner_instalation_ref" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_partner_resale_ref" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_partner_support_ref" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_partner_reps_ref" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_colors_images" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "field_collection_item"
        },
        "field_product_descriptions" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "field_collection_item"
        },
        "field_weight" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "body:value" : { "type" : "text" },
        "body:summary" : { "type" : "text" },
        "field_special_tags:name" : { "type" : "list\\u003Ctext\\u003E", "boost" : "8.0" },
        "field_segments:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_segments:name" : {
          "type" : "list\\u003Ctext\\u003E",
          "real_type" : "list\\u003Ctext_ngram\\u003E"
        },
        "field_videos:file" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "file" },
        "field_colors_images:item_id" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_colors_images:field_colors" : { "type" : "list\\u003Ctext\\u003E" },
        "field_product_descriptions:item_id" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product_descriptions:field_segment_ref" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_segments:parent:name" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_videos:file:fid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_videos:file:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_videos:file:mime" : { "type" : "list\\u003Ctext\\u003E" },
        "field_videos:file:size" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_videos:file:url" : { "type" : "list\\u003Ctext\\u003E" },
        "field_videos:file:timestamp" : { "type" : "list\\u003Cdate\\u003E" },
        "field_colors_images:field_image:file" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "file"
        },
        "field_product_descriptions:field_description:value" : { "type" : "list\\u003Ctext\\u003E" },
        "field_product_descriptions:field_description:summary" : { "type" : "list\\u003Ctext\\u003E" },
        "field_colors_images:field_image:file:fid" : { "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E" },
        "field_colors_images:field_image:file:name" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_colors_images:field_image:file:mime" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_colors_images:field_image:file:size" : { "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E" },
        "field_colors_images:field_image:file:url" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : { "default" : "0", "bundles" : { "product" : "product" } }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 1, "weight" : "0", "settings" : { "fields" : [] } }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_benefits_ref" : true,
              "field_partner_reps_ref" : true,
              "body:value" : true,
              "body:summary" : true,
              "field_product_descriptions:field_description:value" : true,
              "field_product_descriptions:field_description:summary" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : [],
            "title" : 0,
            "alt" : 0,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 0,
          "weight" : "15",
          "settings" : { "fields" : { "title" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  $items['segments'] = entity_import('search_api_index', '{
    "name" : "Segments",
    "machine_name" : "segments",
    "description" : "\\u00cdndice de Segmento de produtos",
    "server" : "intelbras",
    "item_type" : "taxonomy_term",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "tid" : { "type" : "integer" },
        "name" : { "type" : "text", "boost" : "5.0", "real_type" : "text_ngram" },
        "node_count" : { "type" : "integer" },
        "parent" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" },
        "search_api_aggregation_1" : { "type" : "string" },
        "parent:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "parent:name" : {
          "type" : "list\\u003Ctext\\u003E",
          "real_type" : "list\\u003Ctext_ngram\\u003E"
        }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-50",
          "settings" : { "default" : "0", "bundles" : { "segments" : "segments" } }
        },
        "search_api_alter_language_control" : {
          "status" : 1,
          "weight" : "-49",
          "settings" : {
            "lang_field" : "",
            "languages" : { "pt-br" : "pt-br", "en" : "en", "es" : "es" }
          }
        },
        "search_api_alter_add_hierarchy" : {
          "status" : 1,
          "weight" : "-48",
          "settings" : { "fields" : { "parent:parent" : "parent:parent" } }
        },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "-47", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 1,
          "weight" : "-46",
          "settings" : { "fields" : { "search_api_aggregation_1" : {
                "name" : "name_fulltext",
                "type" : "fulltext",
                "fields" : [ "name" ],
                "description" : "A Fulltext aggregation of the following fields: Nome."
              }
            }
          }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "-45", "settings" : { "mode" : "full" } }
      },
      "processors" : {
        "search_api_case_ignore" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "name" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 1,
          "weight" : "15",
          "settings" : { "fields" : { "name" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : { "fields" : [], "spaces" : "[^\\\\p{L}\\\\p{N}]", "ignorable" : "[-]" }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "name" : true },
            "file" : "..\\/config\\/solr\\/stopwords.txt",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}
