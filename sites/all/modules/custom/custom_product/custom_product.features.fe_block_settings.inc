<?php
/**
 * @file
 * custom_product.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function custom_product_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['global_filter-global_filter_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'global_filter_1',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'global_filter',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'intelbras' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'intelbras',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['global_filter-global_filter_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'global_filter_2',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'global_filter',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'intelbras' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'intelbras',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
