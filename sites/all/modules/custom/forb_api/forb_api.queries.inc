<?php



/**
 * Find by subtitle
 * @param $query
 */
function _forb_api_find_products_by_query($key_word) {
    $query = new EntityFieldQuery();
    $result = $query
        ->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'product')
        ->propertyCondition('status', NODE_PUBLISHED)
        ->propertyCondition('language', 'pt-br', '=')
        ->fieldCondition('field_subtitle', 'value', '%' . $key_word . '%', 'LIKE')
        ->execute();
    return $result['node'];
}

function _forbi_api_find_products_nids_by_tids($tids, $limit, $offset) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'product')
        ->propertyCondition('status', NODE_PUBLISHED)
        ->propertyCondition('language', 'pt-br', '=')
        ->fieldCondition('field_segments', 'tid', $tids, 'IN');

    if ($limit && $offset) {
        $query->range($offset, $limit);
    }
    $result = $query->execute();

    $query->range($offset + $limit, $limit);
    $more = $query->count()->execute();

    $data = array(
        'result' => $result['node'],
        'hasMore' => $more,
    );
    return $data;
}

function _forb_api_find_taxonomy_term_by_name($name, $limit, $offset) {

    $query = new EntityFieldQuery();
    $query
        ->entityCondition('entity_type', 'taxonomy_term')
        ->propertyCondition('language', 'pt-br', '=')
        ->propertyCondition('name', '%' . $name . '%' , 'LIKE');

    if ($limit && $offset) {
        $query->range($offset, $limit);
    }
    $result = $query->execute();

    $query->range($offset + $limit, $limit);
    $more = $query->count()->execute();

    $data = array(
        'result' => $result['taxonomy_term'],
        'hasMore' => $more,
    );

    return $data;
}

function _forb_api_find_all_states() {
    $query_string = "
        SELECT 
	address.field_address_administrative_area AS code_state 
	FROM field_data_field_address AS address
    LEFT JOIN node AS node ON (node.nid = address.entity_id)
    LEFT JOIN field_data_field_partner_type AS partner_type ON (partner_type.entity_id = address.entity_id)
	WHERE address.bundle = 'partner' 
	AND partner_type.field_partner_type_value = 'resale' 
	AND address.field_address_country = 'BR' 
	AND address.field_address_administrative_area <> '' 
    AND node.status = 1
	GROUP BY address.field_address_administrative_area";
    $query = db_query($query_string);
    return $query->fetchAll();
}

function _forb_api_find_cities_by_state($code_state) {
    $query_string = "
        SELECT address.field_address_locality 
        FROM field_data_field_address AS address
        LEFT JOIN node AS node ON (node.nid = address.entity_id)
        LEFT JOIN field_data_field_partner_type AS partner_type ON (partner_type.entity_id = address.entity_id)
        WHERE address.bundle = 'partner'  
        AND partner_type.field_partner_type_value = 'resale' 
        AND address.field_address_administrative_area = '$code_state'  
        AND address.field_address_locality <> ''
        AND node.status = 1
        GROUP BY address.field_address_locality";

    $query = db_query($query_string);
    return $query->fetchAll();
}

function _forb_api_find_partners_by_city($city, $partner_type, $limit, $offset) {
    $query_string = "
	SELECT 
	node.title,
	address.field_address_organisation_name, 
    address.field_address_administrative_area, 
    address.field_address_thoroughfare,
    address.field_address_premise, 
    address.field_address_locality, 
    address.field_address_data,
    address.field_address_postal_code,
    email.field_email_email AS email
	FROM field_data_field_address AS address
    LEFT JOIN node AS node ON (node.nid = address.entity_id)
    LEFT JOIN field_data_field_email AS email ON (email.entity_id = address.entity_id)
    LEFT JOIN field_data_field_partner_type AS partner_type ON (partner_type.entity_id = address.entity_id)
	WHERE address.bundle = 'partner'  
    AND partner_type.field_partner_type_value = '$partner_type' 
	AND address.field_address_locality = '$city'
    AND node.status = 1";

    $query_string_result = $query_string;

    if ($limit && $offset) {
      $query_string_result .= " LIMIT $limit OFFSET $offset";
    }

    $query = db_query($query_string_result);
    $result = $query->fetchAll();

    $query_string_count = $query_string;
    $query_string_count .= " LIMIT " . (int) $limit . " OFFSET " . (int) ($offset + $limit);
    $result_count = db_query($query_string_count);
    $count = $result_count->fetchAll();

    $data = array(
        'result' => $result,
        'hasMore' => count($count)
    );
    return $data;
}

function _forb_api_get_colors_by_ids($ids) {
    if (!$ids) {
        return array();
    }
    $query_string = "SELECT field_colors_jquery_colorpicker 
      FROM field_data_field_colors WHERE entity_id IN (". implode($ids, ',') . ")";
    $query = db_query($query_string);
    $result =  $query->fetchAll();
    $output = array();
    foreach($result as $row) {
        $output[] = $row->field_colors_jquery_colorpicker;
    }
    return $output;
}

function _forb_api_get_images_by_colors_ids($colors_ids) {

    if (!$colors_ids) {
        return array();
    }
    $query_string = "SELECT field_image_fid FROM field_data_field_image WHERE entity_id IN (" . implode($colors_ids, ',') . ")";
    $query = db_query($query_string);
    $result = $query->fetchAll();
    $output = array();
    foreach ($result as $row) {
        $file = file_load($row->field_image_fid);
        $output[] = file_create_url($file->uri);
    }
    return $output;
}

// Only 0. TODO Confirm validation
function _forb_api_get_description_by_id($id) {

    $query_string = "SELECT field_description_value FROM field_data_field_description WHERE entity_id = $id";
    $query = db_query($query_string);
    $result = $query->fetchAssoc();
    $output = $result['field_description_value'];
    $output = strip_tags($output);
    return $output;
}

function _forb_api_build_tech_specification($techSpeci_tids) {
    $techSpeci_terms = taxonomy_term_load_multiple($techSpeci_tids);
    $techSpeci_array = array();
    foreach($techSpeci_terms as $term) {
        $parent = array_shift(taxonomy_get_parents($term->tid));
        $techSpeci_array[$parent->name][] = $term->name;
    }
    return $techSpeci_array;
}

function _forb_api_get_reference($product_id, $type, $uuid = null) {
    $query_string = "SELECT reference.entity_id FROM field_data_field_product_reference AS reference
        LEFT JOIN field_data_field_download_type AS download_type ON (download_type.entity_id = reference.entity_id)
        LEFT JOIN taxonomy_term_data AS term_data ON (term_data.tid = download_type.field_download_type_tid)
        WHERE reference.field_product_reference_target_id = '$product_id' 
        AND reference.bundle = '$type'";

    switch ($type) {
        case "download":
            if ($uuid == '30ce185c-7956-46a4-8ca5-be2bbb4f7353') {
                $query_string .= " AND term_data.uuid = '30ce185c-7956-46a4-8ca5-be2bbb4f7353'";
            } else {
                $query_string .= " AND term_data.uuid != '30ce185c-7956-46a4-8ca5-be2bbb4f7353'";
            }
        break;
        default;
            break;
    }

    $query = db_query($query_string);
    return $query->fetchAll();
}
